# Use an OpenJDK image that is compatible with OpenShift's arbitrary user ID policy
FROM openjdk:17-jdk-alpine

# Argument for Maven version
ARG MAVEN_VERSION=3.9.6
ARG USER_HOME_DIR="/root"
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

# Install necessary tools
RUN yum install -y curl && \
    mkdir -p /usr/share/maven /usr/share/maven/ref && \
    curl -fsSL -o /tmp/apache-maven.tar.gz ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz && \
    tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 && \
    rm -f /tmp/apache-maven.tar.gz && \
    ln -s /usr/share/maven/bin/mvn /usr/bin/mvn && \
    # Ensure the root group has access to necessary directories for OpenShift's arbitrary UID support
    chgrp -R 0 /usr/share/maven /usr/share/maven/ref ${USER_HOME_DIR} && \
    chmod -R g=u /usr/share/maven /usr/share/maven/ref ${USER_HOME_DIR} && \
    yum clean all

# Environment variables for Maven and Java
ENV MAVEN_HOME=/usr/share/maven \
    MAVEN_CONFIG="${USER_HOME_DIR}/.m2" \

# Create and set permissions for the /data directory and app.log
RUN mkdir -p /data && \
    touch /data/app.log && \
    chgrp -R 0 /data && \
    chmod -R g=u /data

# Switch to the /data directory as a working directory
WORKDIR /data

# Copy the JAR file and entrypoint script into the image
COPY ./KKI-0.0.1-SNAPSHOT.jar /data/
COPY entrypoint.sh /entrypoint.sh

# Ensure the entrypoint script and JAR file are executable
RUN chmod +x /entrypoint.sh && \
    chmod +x /data/KKI-0.0.1-SNAPSHOT.jar

# Expose the port your app runs on
EXPOSE 8085

# Set the entrypoint script to be executed
ENTRYPOINT ["/entrypoint.sh"]

# Ensure the image can be run with an arbitrary user ID by setting the user to non-root
USER 1001